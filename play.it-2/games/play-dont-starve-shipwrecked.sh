#!/bin/sh -e
set -o errexit

###
# Copyright (c) 2015-2018, Antoine Le Gonidec
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# This software is provided by the copyright holders and contributors "as is"
# and any express or implied warranties, including, but not limited to, the
# implied warranties of merchantability and fitness for a particular purpose
# are disclaimed. In no event shall the copyright holder or contributors be
# liable for any direct, indirect, incidental, special, exemplary, or
# consequential damages (including, but not limited to, procurement of
# substitute goods or services; loss of use, data, or profits; or business
# interruption) however caused and on any theory of liability, whether in
# contract, strict liability, or tort (including negligence or otherwise)
# arising in any way out of the use of this software, even if advised of the
# possibility of such damage.
###

###
# Don’t Starve: Shipwrecked
# build native Linux packages from the original installers
# send your bug reports to vv221@dotslashplay.it
###

script_version=20180212.1

# Set game-specific variables

GAME_ID='dont-starve'
GAME_NAME='Don’t Starve - Shipwrecked'

ARCHIVES_LIST='ARCHIVE_GOG ARCHIVE_GOG_OLD ARCHIVE_GOG_OLDER'

ARCHIVE_GOG='don_t_starve_shipwrecked_dlc_en_20171215_17628.sh'
ARCHIVE_GOG_MD5='463825173d76f294337f0ae7043d7cf6'
ARCHIVE_GOG_SIZE='1200000'
ARCHIVE_GOG_TYPE='mojosetup'
ARCHIVE_GOG_VERSION='20171215-gog17628'

ARCHIVE_GOG_OLD='gog_don_t_starve_shipwrecked_dlc_2.0.0.2.sh'
ARCHIVE_GOG_OLD_MD5='b1d4152639a272a959d36eacf8cb859e'
ARCHIVE_GOG_OLD_SIZE='1200000'
ARCHIVE_GOG_OLD_VERSION='gog2.0.0.2'

ARCHIVE_DOC_DATA_PATH='data/noarch/docs'
ARCHIVE_DOC_DATA_FILES='./*'

ARCHIVE_GAME_DATA_PATH='data/noarch/game/dontstarve32'
ARCHIVE_GAME_DATA_FILES='./data ./manifest_dlc0002.json'

PACKAGES_LIST='PKG_DATA'

PKG_DATA_ID="${GAME_ID}-data-shipwrecked"
PKG_DATA_DESCRIPTION='data shipwrecked'

PKG_BIN32_ARCH='32'
PKG_BIN32_DEPS="$PKG_DATA_ID libcurl-gnutls glu sdl2"

PKG_BIN64_ARCH='64'
PKG_BIN64_DEPS="$PKG_BIN32_DEPS"

# Load common functions

target_version='2.4'

if [ -z "$PLAYIT_LIB2" ]; then
	[ -n "$XDG_DATA_HOME" ] || XDG_DATA_HOME="$HOME/.local/share"
	if [ -e "$XDG_DATA_HOME/play.it/play.it-2/lib/libplayit2.sh" ]; then
		PLAYIT_LIB2="$XDG_DATA_HOME/play.it/play.it-2/lib/libplayit2.sh"
	elif [ -e './libplayit2.sh' ]; then
		PLAYIT_LIB2='./libplayit2.sh'
	else
		printf '\n\033[1;31mError:\033[0m\n'
		printf 'libplayit2.sh not found.\n'
		return 1
	fi
fi
. "$PLAYIT_LIB2"

# Extract game data

extract_data_from "$SOURCE_ARCHIVE"

for PKG in $PACKAGES_LIST; do
	organize_data "DOC_${PKG#PKG_}"  "$PATH_DOC"
	organize_data "GAME_${PKG#PKG_}" "$PATH_GAME"
done

rm --recursive "$PLAYIT_WORKDIR/gamedata"

# Build package

write_metadata 'PKG_DATA'
build_pkg

# Clean up

rm --recursive "$PLAYIT_WORKDIR"

# Print instructions

printf '\n'
printf '32-bit:'
print_instructions 'PKG_DATA'
printf '64-bit:'
print_instructions 'PKG_DATA'

exit 0
